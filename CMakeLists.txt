cmake_minimum_required(VERSION 3.16)
project(kxcjvm)

set(CMAKE_CXX_STANDARD 17)

#set(LIB_DIR D:/Development/programming/gru/clibs)
#set(LIB_DIR e:/Projects/c++/libs)
#file(GLOB_RECURSE SRC CONFIGURE_DEPENDS src/*.cpp )


#include_directories(xrender-lib
#		)
#link_directories(koenig ${LIB_DIR}/SDL2-devel-2.0.14-VC/SDL2-2.0.14/lib/x64 
#						${LIB_DIR}/glew-2.2.0/lib/Release/x64 
##						${LIB_DIR}/assimp-5.0.1/assimp-5.0.1/build/code/Release
#						${LIB_DIR}/assimp-5.0.1/assimp-5.0.1/build/code/Debug
#						${LIB_DIR}/freetype-2.10.4/build/Debug
#						${LIB_DIR}/SDL2_mixer-2.0.4/lib/x64
#						)

if (WIN32) 
	set(SRC_LIB "src/win32/xrender_win32.cpp")
elseif(APPLE)
	# TODO
elseif(UNIX)
	# TODO
endif()



#add_library(xrender src/common/xrender.cpp ${SRC_LIB})
add_executable(kxjava src/main.cpp src/ClassLoader.cpp src/ClassLoader.h src/ClassFile.cpp src/ClassFile.h src/ConstantPoolItem.cpp src/ConstantPoolItem.h)

#target_link_libraries(xrender-test xrender)


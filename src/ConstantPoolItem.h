//
// Created by mgrus on 22/12/2021.
//

#ifndef KXCJVM_CONSTANTPOOLITEM_H
#define KXCJVM_CONSTANTPOOLITEM_H


#include <cstdint>

class ConstantPoolItem {
public:
    uint8_t tag;

};

class CPClassInfo : public ConstantPoolItem {
public:
    int nameIndex;
};

class CPFieldMethodInterfaceInfo : public ConstantPoolItem {
public:
    int classIndex;
    int nameAndTypeIndex;
};

class CPString : public ConstantPoolItem {
public:
    int stringIndex;
};

class CPIntegerFloatInfo : public ConstantPoolItem {
public:
    uint8_t bytes[4];
};

class CPLongDoubleInfo : public ConstantPoolItem {
public:
    uint8_t hiBytes[4];
    uint8_t loBytes[4];
};

class CPUTF8Info : public ConstantPoolItem {
public:
    int len;
    uint8_t* bytes;
};

class CPMethodHandleInfo : public ConstantPoolItem {
public:
    int refKind;
    int refIndex;
};

class CPMethodTypeInfo : public ConstantPoolItem {
public:
    int descriptorIndex;
};

class CDynamicInfo : public ConstantPoolItem {
public:
    int bootstrapMethodAttribute;
    int nameAndTypeIndex;
};

class CPModulePackageInfo : public ConstantPoolItem {
public:
    int nameIndex;
};

class CPNameAndTypeInfo : public ConstantPoolItem {
public:
    int nameIndex;
    int descriptorIndex;
};





#endif //KXCJVM_CONSTANTPOOLITEM_H

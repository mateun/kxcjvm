//
// Created by mgrus on 22/12/2021.
//

#ifndef KXCJVM_CLASSLOADER_H
#define KXCJVM_CLASSLOADER_H


#include <string>
#include "ClassFile.h"

class ClassLoader {

public:
    ClassFile* loadClass(const std::string& className) ;
private:
    uint16_t wordFromBytes(uint8_t* bytes);
};


#endif //KXCJVM_CLASSLOADER_H

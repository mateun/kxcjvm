//
// Created by mgrus on 22/12/2021.
//

#include "ClassLoader.h"
#include <cstdio>

/*

 see https://docs.oracle.com/javase/specs/jvms/se12/html/jvms-4.html
 ClassFile {
    u4             magic;
    u2             minor_version;
    u2             major_version;
    u2             constant_pool_count;
    cp_info        constant_pool[constant_pool_count-1];
    u2             access_flags;
    u2             this_class;
    u2             super_class;
    u2             interfaces_count;
    u2             interfaces[interfaces_count];
    u2             fields_count;
    field_info     fields[fields_count];
    u2             methods_count;
    method_info    methods[methods_count];
    u2             attributes_count;
    attribute_info attributes[attributes_count];
}

 */


ClassFile *ClassLoader::loadClass(const std::string &className) {
    ClassFile* classFile = new ClassFile();
    FILE* file;
    fopen_s(&file, className.c_str(), "r");
    uint8_t magic[4];
    //fread_s(magic, 4, 1, 4, file);
    fread(magic, 1, 4, file);
    if (!(magic[0] == 0xCA && magic[1] == 0xFE && magic[2] == 0xBA && magic[3] == 0xBE)) {
        printf("wrong magic value");
        exit(1);
    }

    uint8_t minorB[2];
    uint8_t majorB[2];
    uint16_t major = 0;
    uint16_t minor = 0;
    fread(&minorB, 1, 2, file);
    fread(majorB, 1, 2, file);
    minor = wordFromBytes(minorB);
    major = wordFromBytes(majorB);

    uint16_t cpCount = 0;
    uint8_t cpCountB[2];
    fread(cpCountB, 1, 2, file);
    cpCount = wordFromBytes(cpCountB);

    return nullptr;
}

// Convertes big-endian bytes to word (16 bit)
uint16_t ClassLoader::wordFromBytes(uint8_t *bytes) {
    return bytes[0] << 8 | bytes[1];
}

//
// Created by mgrus on 22/12/2021.
//

#ifndef KXCJVM_CLASSFILE_H
#define KXCJVM_CLASSFILE_H


#include <vector>
#include "ConstantPoolItem.h"

class ClassFile {

public:
    int minorVersion;
    int majorVersion;
    std::vector<ConstantPoolItem> constantPool;


};


#endif //KXCJVM_CLASSFILE_H
